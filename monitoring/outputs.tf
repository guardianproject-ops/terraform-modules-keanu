output "monitoring" {
  value = module.monitoring
}

output "matrix_alertmanager_lambda" {
  value = module.matrix_alertmanager
}

output "prometheus_ami" {
  value = local.ami
}

output "cloudflare_zone_id" {
  value = var.cloudflare_zone_id
}

output "cloudflare_application_id" {
  value = cloudflare_access_application.monitor.id
}

