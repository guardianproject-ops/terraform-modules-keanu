variable "key_name" {
  type        = string
  description = "The ssh key name"
}

variable "ma1sd_internal_domain" {
  type = string
}

variable "synapse_internal_domain" {
  type = string
}

variable "sygnal_internal_domain" {
  type = string
}
variable "matrix_domain" {
  type        = string
  description = "The full external matrix domain"
}

variable "matrix_web_domain" {
  type        = string
  description = "The full external domain to the web client"
}
variable "cloudflare_origin_ca_key" {
  type = string
}
variable "cloudflare_access_edit_api_token" {
  type        = string
  description = "CF API Access Token that has edit permissions on the cloudflare zone"
}
variable "cloudflare_auth_key" {
  type = string
}
variable "cloudflare_auth_email" {
  type = string
}
variable "cloudflare_zone" {
  type        = string
  description = "The CF zone name used to expose monitoring tools. E.g., `example.com`. Must match `cloudflare_zone_id`."
}
variable "cloudflare_zone_id" {
  type        = string
  description = "The CF zone id used to expose monitoring tools. `monitoring_domain` must exist under this zone."
}
variable "monitoring_domain" {
  type        = string
  description = "The full domain monitoring tools are exposed at. e.g., monitor.example.com. Must exist under the zone specified by `cloudflare_zone_id`"
}
variable "cloudflare_access_admin_groups" {
  type        = list(string)
  description = "The CF Access group ids for admin users"
}
variable "alertmanager_receivers" {
}
variable "alertmanager_route" {
}
variable "matrix_alertmanager_shared_secret" {
  type        = string
  description = "a shared secret used by alertmanager and this lambda, it must be URL safe."
}
variable "matrix_alertmanager_homeserver_url" {
  type        = string
  description = "your alert-receiving homeserver url, example: https://matrix-client.matrix.org"
}
variable "matrix_alertmanager_rooms" {
  type        = list(any)
  description = "A list of rooms in the format  <room name>/!XXROOMID:matrix.org."
}

variable "matrix_alertmanager_receiver" {
  type        = string
  description = "The alertmanager receiver that cloudwatch alarms go to"
}
variable "matrix_alertmanager_user_token" {
  type        = string
  description = "The api token for your matrix bot user"
}
variable "matrix_alertmanager_user_id" {
  type        = string
  description = "The matrix id of your matrix bot user, example: @myfoobot:matrix.org"
}
variable "ssm_logs_bucket" {
  type        = string
  description = "S3 bucket name of the bucket where SSM Session Manager logs are stored"
}

variable "kms_key_arn" {
  type        = string
  description = "kms key arn to encrypt prometheus data with, if none provided one will be created."
  default     = ""
}
variable "vpc_id" {
  type        = string
  description = "the vpc id the instance will be placed in"
}
variable "az" {
  type        = string
  description = "the AZ prometheus will live in"
}

variable "ami" {
  type        = string
  description = "AMI id for the instance"
  default     = ""
}

variable "instance_type" {
  type        = string
  default     = "t3.nano"
  description = "ec2 instance type"
}

variable "subnet_id" {
  description = "the subnet id to place the instance on"
  type        = string
}


variable "ami_filter" {
  type    = map(string)
  default = {}
}

variable "sns_topic_subscription_https_endpoint_critical" {
  type        = string
  description = "the sns subscription https endpoint (e.g, your victorops cloudwatch url)"
}

variable "rds_instance_id" {
  type        = string
  description = "The id of the rds database to monitor"
}

variable "alb_id" {
  type        = string
  description = "The id of the ALB to monitor"
}

variable "failed_ansible_runs_threshold" {
  description = "The number of failed ansible runs overwhich will trigger an alert"
  type        = number
  default     = 1
}
variable "rds_cpu_utilization_threshold" {
  description = "The maximum percentage of CPU utilization."
  type        = number
  default     = 80
}

variable "rds_freeable_memory_threshold" {
  description = "The minimum amount of available random access memory in bytes"
  type        = number
  default     = 128000000 # 128mb in bytes
}

variable "rds_free_storage_space_threshold" {
  description = "The minimum amount of available storage space in bytes"
  type        = number
  default     = 3000000000 # 3gb in bytes
}

variable "loadbalancer_5xx_threshold" {
  description = "The number of 5xx in 10 minutes over which will trigger an alarm"
  type        = number
  default     = 100
}

variable "logs_missing_threshold_sygnal" {
  description = "The number of log events in 2 hours under which will trigger an alarm"
  type        = string
  default     = 1
}
variable "logs_missing_threshold_ma1sd" {
  description = "The number of log events in 2 hours under which will trigger an alarm"
  type        = string
  default     = 1
}
variable "logs_missing_threshold_synapse" {
  description = "The number of log events in 2 hours under which will trigger an alarm"
  type        = string
  default     = 1
}

variable "logs_missing_threshold_journald" {
  description = "The number of log events in 2 hours under which will trigger an alarm"
  type        = string
  default     = 1
}

variable "log_group_journald" {
  type        = string
  description = "the log group name for journald logs"
}

variable "log_group_synapse" {
  type        = string
  description = "the log group name for synapse logs"
}
variable "log_group_sygnal" {
  type        = string
  description = "the log group name for sygnal logs"
}
variable "log_group_ma1sd" {
  type        = string
  description = "the log group name for ma1sd logs"
}

variable "log_retention_days" {
  type        = number
  default     = 7
  description = "how many days the prometheus, alertmanager, and grafana logs should be kept"
}
