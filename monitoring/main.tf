##
## Monitoring
## Creates:
##  * SNS Topic for sending alerts to VictorOps
##  * Various Cloudwatch alarms for monitoring the matrix stack
##  * Prometheus instance for gathering instance stats
##  * matrix-alertmanager lambda for sending messages to matrix
##  * lambda that forwards cloudwatch alarms to matrix
##  * cloudflare access policies for prometheus ui
##

data "aws_region" "current" {}

##############################
# AMI Lookup                 #
##############################
locals {
  default_ami_filter = {
    "Application" : "debian-base",
    "Namespace" : module.this.namespace,
    "Environment" : module.this.environment
  }
  ami_filter = length(var.ami_filter) > 0 ? var.ami_filter : local.default_ami_filter
  ami        = coalesce(var.ami, data.aws_ami.debian.id)
}

data "aws_ami" "debian" {
  most_recent = true
  owners      = ["self"]

  dynamic "filter" {
    for_each = local.ami_filter
    iterator = tag

    content {
      name   = "tag:${tag.key}"
      values = ["${tag.value}"]
    }
  }
}

##############################
# Matrix Alertmanager Lambda #
##############################
# forwards alertmanager alerts to a matrix channel
module "matrix_alertmanager" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-lambda-matrix-alertmanager.git?ref=tags/2.1.0"

  attributes                         = ["matrix-alertmanager"]
  context                            = module.this.context
  matrix_alertmanager_shared_secret  = var.matrix_alertmanager_shared_secret
  matrix_alertmanager_homeserver_url = var.matrix_alertmanager_homeserver_url
  matrix_alertmanager_rooms          = var.matrix_alertmanager_rooms
  matrix_alertmanager_user_token     = var.matrix_alertmanager_user_token
  matrix_alertmanager_user_id        = var.matrix_alertmanager_user_id
}

# forwards cloudwatch alarms to the matrix alertmanager lambda
module "cloudwatch_matrix" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-lambda-cloudwatch-matrix-alertmanager.git?ref=tags/0.1.1"

  context                      = module.this.context
  attributes                   = ["cloudwatch-matrix"]
  matrix_alertmanager_url      = "${module.matrix_alertmanager.lambda_url}/alerts?secret=${var.matrix_alertmanager_shared_secret}"
  matrix_alertmanager_receiver = var.matrix_alertmanager_receiver
  sns_topic_arns               = [aws_sns_topic.warning.arn, aws_sns_topic.critical.arn]
  build_number                 = 2
}

#########################
# Cloudwatch Log Groups #
#########################

# log groups for the apps to log into

module "label_log_prometheus" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  attributes = ["prometheus"]
  delimiter  = "/"
  context    = module.this.context
}

resource "aws_cloudwatch_log_group" "prometheus" {
  name              = "/${module.label_log_prometheus.id}"
  retention_in_days = var.log_retention_days
}

module "label_log_grafana" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  attributes = ["grafana"]
  delimiter  = "/"
  context    = module.this.context
}

resource "aws_cloudwatch_log_group" "grafana" {
  name              = "/${module.label_log_grafana.id}"
  retention_in_days = var.log_retention_days
}

module "label_log_alertmanager" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  attributes = ["alertmanager"]
  delimiter  = "/"
  context    = module.this.context
}

resource "aws_cloudwatch_log_group" "alertmanager" {
  name              = "/${module.label_log_alertmanager.id}"
  retention_in_days = var.log_retention_days
}

#########################
# EC2                   #
#########################

# sets up the ec2 resources for prometheus, alertmanager and grafana
module "monitoring" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-prometheus?ref=tags/3.0.0"

  context                           = module.this.context
  cloudflare_origin_ca_key          = var.cloudflare_origin_ca_key
  cloudflare_auth_key               = var.cloudflare_auth_key
  cloudflare_auth_email             = var.cloudflare_auth_email
  cloudflare_zone                   = var.cloudflare_zone
  cloudflare_zone_id                = var.cloudflare_zone_id
  monitoring_domain                 = var.monitoring_domain
  matrix_alertmanager_shared_secret = var.matrix_alertmanager_shared_secret
  matrix_alertmanager_url           = module.matrix_alertmanager.lambda_url
  alertmanager_receivers            = replace(var.alertmanager_receivers, "matrix_alertmanager_url", module.matrix_alertmanager.lambda_url)
  alertmanager_route                = var.alertmanager_route
  kms_key_arn                       = var.kms_key_arn
  key_name                          = var.key_name

  ami             = local.ami
  vpc_id          = var.vpc_id
  az              = var.az
  instance_type   = var.instance_type
  subnet_id       = var.subnet_id
  ssm_logs_bucket = var.ssm_logs_bucket

  extra_ssm_params = {
    "ma1sd_internal_domain" : var.ma1sd_internal_domain,
    "sygnal_internal_domain" : var.sygnal_internal_domain,
    "synapse_internal_domain" : var.synapse_internal_domain,
    "matrix_domain" : var.matrix_domain,
    "matrix_web_domain" : var.matrix_web_domain,
    "log_group_journald" : var.log_group_journald,
    "log_group_alertmanager" : aws_cloudwatch_log_group.alertmanager.name,
    "log_group_prometheus" : aws_cloudwatch_log_group.prometheus.name,
    "log_group_grafana" : aws_cloudwatch_log_group.grafana.name
  }
}

#########################
# Cloudwatch SNS        #
#########################

# creates the sns resources for piping alerts to their destination
module "label_cloudwatch" {
  source = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"

  attributes = ["cloudwatch", data.aws_region.current.name]
  context    = module.this.context
}

module "label_crit" {
  source = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"

  attributes = ["crit"]
  context    = module.label_cloudwatch.context
}

module "label_warn" {
  source = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"

  attributes = ["warn"]
  context    = module.label_cloudwatch.context
}

# the sns topic for critical alerts, sent to both victorops and matrix
resource "aws_sns_topic" "critical" {
  name = module.label_crit.id
  tags = module.label_crit.tags
}

resource "aws_sns_topic_subscription" "crit" {
  topic_arn              = aws_sns_topic.critical.arn
  protocol               = "https"
  endpoint               = var.sns_topic_subscription_https_endpoint_critical
  endpoint_auto_confirms = true
}

resource "aws_sns_topic_subscription" "crit_matrix" {
  topic_arn = aws_sns_topic.critical.arn
  protocol  = "lambda"
  endpoint  = module.cloudwatch_matrix.lambda_arn
}

# the sns topic for warning alerts
resource "aws_sns_topic" "warning" {
  name = module.label_warn.id
  tags = module.label_warn.tags
}

resource "aws_sns_topic_subscription" "warn" {
  topic_arn = aws_sns_topic.warning.arn
  protocol  = "lambda"
  endpoint  = module.cloudwatch_matrix.lambda_arn
}

#########################
# Cloudwatch Alarms     #
#########################

locals {
  thresholds = {
    FailedAnsibleRunsThreshold   = max(var.failed_ansible_runs_threshold, 0)
    RDSCPUUtilizationThreshold   = min(max(var.rds_cpu_utilization_threshold, 0), 100)
    RDSFreeableMemoryThreshold   = max(var.rds_freeable_memory_threshold, 0)
    RDSFreeStorageSpaceThreshold = max(var.rds_free_storage_space_threshold, 0)
    LoadBalancer5XXThreshold     = max(var.loadbalancer_5xx_threshold, 0)
    SynapseLogsMissing           = min(var.logs_missing_threshold_synapse, 1)
    Ma1sdLogsMissing             = min(var.logs_missing_threshold_ma1sd, 1)
    SygnalLogsMissing            = min(var.logs_missing_threshold_sygnal, 1)
  }
}

resource "aws_cloudwatch_metric_alarm" "failed_commands_alarm" {
  alarm_name        = "Failed Ansible Runs Detected"
  alarm_description = "The number of failed SSM RunCommands in the past 10 minutes"

  namespace           = "AWS/SSM-RunCommand"
  metric_name         = "CommandsFailed"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  datapoints_to_alarm = 1
  period              = 600
  statistic           = "Sum"
  threshold           = local.thresholds["FailedAnsibleRunsThreshold"]
  treat_missing_data  = "notBreaching"
  alarm_actions       = [aws_sns_topic.warning.arn]
  ok_actions          = [aws_sns_topic.warning.arn]
  tags                = module.label_cloudwatch.tags
}

resource "aws_cloudwatch_metric_alarm" "rds_cpu_utilization_too_high" {
  alarm_name          = "RDS cpu utilization too high"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = 600
  statistic           = "Average"
  threshold           = local.thresholds["RDSCPUUtilizationThreshold"]
  alarm_description   = "Average database CPU utilization over last 10 minutes too high"
  alarm_actions       = [aws_sns_topic.warning.arn]
  ok_actions          = [aws_sns_topic.warning.arn]
  tags                = module.label_cloudwatch.tags

  dimensions = {
    DBInstanceIdentifier = var.rds_instance_id
  }
}


resource "aws_cloudwatch_metric_alarm" "rds_freeable_memory_too_low" {
  alarm_name          = "RDS memory too low"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeableMemory"
  namespace           = "AWS/RDS"
  period              = 600
  statistic           = "Average"
  threshold           = local.thresholds["RDSFreeableMemoryThreshold"]
  alarm_description   = "Average database freeable memory over last 10 minutes too low, performance may suffer"
  alarm_actions       = [aws_sns_topic.warning.arn]
  ok_actions          = [aws_sns_topic.warning.arn]
  tags                = module.label_cloudwatch.tags

  dimensions = {
    DBInstanceIdentifier = var.rds_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_free_storage_space_too_low" {
  alarm_name          = "RDS free storage space threshold"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = 600
  statistic           = "Average"
  threshold           = local.thresholds["RDSFreeStorageSpaceThreshold"]
  alarm_description   = "Average database free storage space over last 10 minutes too low"
  alarm_actions       = [aws_sns_topic.warning.arn]
  ok_actions          = [aws_sns_topic.warning.arn]
  tags                = module.label_cloudwatch.tags

  dimensions = {
    DBInstanceIdentifier = var.rds_instance_id
  }
}


resource "aws_cloudwatch_metric_alarm" "load_balancer_app_5xxs_too_high" {
  alarm_name          = "Load Balancer5xxs too high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "HTTPCode_ELB_5XX_Count"
  namespace           = "AWS/ApplicationELB"
  period              = 600
  statistic           = "Sum"
  threshold           = local.thresholds["LoadBalancer5XXThreshold"]
  treat_missing_data  = "notBreaching"
  alarm_description   = "Too many 5xx status responses received from application over the last 10 minutes"
  alarm_actions       = [aws_sns_topic.critical.arn]
  ok_actions          = [aws_sns_topic.critical.arn]
  tags                = module.label_cloudwatch.tags

  dimensions = {
    LoadBalancer = var.alb_id
  }
}

resource "aws_cloudwatch_metric_alarm" "missing_logs_synapse" {
  alarm_name          = "SynapseLogsMissing"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "IncomingLogEvents"
  namespace           = "AWS/Logs"
  period              = 7200
  statistic           = "Sum"
  threshold           = local.thresholds["SynapseLogsMissing"]
  treat_missing_data  = "breaching"
  alarm_description   = "No synapse logs received for over two hours"
  alarm_actions       = [aws_sns_topic.warning.arn]
  ok_actions          = [aws_sns_topic.warning.arn]
  tags                = module.label_cloudwatch.tags

  dimensions = {
    LogGroupName = var.log_group_synapse
  }
}

resource "aws_cloudwatch_metric_alarm" "missing_logs_sygnal" {
  alarm_name          = "SygnalLogsMissing"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "IncomingLogEvents"
  namespace           = "AWS/Logs"
  period              = 7200
  statistic           = "Sum"
  threshold           = local.thresholds["SygnalLogsMissing"]
  treat_missing_data  = "breaching"
  alarm_description   = "No sygnal logs received for over two hours"
  alarm_actions       = [aws_sns_topic.warning.arn]
  ok_actions          = [aws_sns_topic.warning.arn]
  tags                = module.label_cloudwatch.tags

  dimensions = {
    LogGroupName = var.log_group_sygnal
  }
}

resource "aws_cloudwatch_metric_alarm" "missing_logs_ma1sd" {
  alarm_name          = "Ma1sdLogsMissing"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "IncomingLogEvents"
  namespace           = "AWS/Logs"
  period              = 7200
  statistic           = "Sum"
  threshold           = local.thresholds["Ma1sdLogsMissing"]
  treat_missing_data  = "breaching"
  alarm_description   = "No ma1sd logs received for over two hours"
  alarm_actions       = [aws_sns_topic.warning.arn]
  ok_actions          = [aws_sns_topic.warning.arn]
  tags                = module.label_cloudwatch.tags

  dimensions = {
    LogGroupName = var.log_group_ma1sd
  }
}

#########################
# Cloudflare Access     #
#########################

module "label_cf" {
  source = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"

  context = module.this.context
}

provider "cloudflare" {
  version   = "~> 2.0"
  api_token = var.cloudflare_access_edit_api_token
}

resource "cloudflare_access_application" "monitor" {
  zone_id                   = var.cloudflare_zone_id
  name                      = module.label_cf.id
  domain                    = var.monitoring_domain
  session_duration          = "6h"
  auto_redirect_to_identity = true
}

resource "cloudflare_access_policy" "admins" {
  application_id = cloudflare_access_application.monitor.id
  zone_id        = var.cloudflare_zone_id
  name           = "allow admins"
  precedence     = "1"
  decision       = "allow"

  include {
    group = var.cloudflare_access_admin_groups
  }
}

resource "cloudflare_access_policy" "deny" {
  application_id = cloudflare_access_application.monitor.id
  zone_id        = var.cloudflare_zone_id
  name           = "deny everyone"
  precedence     = "2"
  decision       = "deny"

  include {
    everyone = true
  }
}
