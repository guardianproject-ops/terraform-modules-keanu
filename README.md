# !! Deprecated !!

**This repo has been superseded** by [guardianproject-ops/terraform-modules-operations](https://gitlab.com/guardianproject-ops/terraform-modules-operations), where we have consolidated our root modules into one repo.


# terraform-modules-keanu

A collection of terraform modules for Keanu, a secure and scalable Matrix
deployment.

To learn how to use this repository, head to [docs.keanu.im](http://docs.keanu.im).

Related repos

* https://gitlab.com/keanuapp/keanu-reference-deployment
* https://gitlab.com/guardianproject-ops/ansible-playbooks-keanu
* https://gitlab.com/guardianproject-ops/packer-keanu/


# License

All code in this repo is licensed under the [GNU Affero General Public License
(AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html), unless other wise
stated.

# Author Information

* Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)


