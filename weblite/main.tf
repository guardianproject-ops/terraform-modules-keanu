provider "aws" {
  # CloudFront requires that Lambda@Edge be in us-east-1
  alias  = "us_east_1"
  region = "us-east-1"
}

provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

resource "cloudflare_record" "this" {
  zone_id = var.cloudflare_zone_id
  name    = var.domain_name
  value   = aws_cloudfront_distribution.this.domain_name
  type    = "CNAME"
  ttl     = 1800
}

resource "aws_cloudfront_distribution" "this" {
  aliases = [var.domain_name]

  origin {
    domain_name = var.origin_domain_name
    origin_id   = "${module.this.id}-origin"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.this.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${module.this.id}-origin"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 0 # 3600
    max_ttl                = 0 # 86400

    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = module.lambda_origin_request.lambda_function_qualified_arn
    }

    lambda_function_association {
      event_type = "origin-response"
      lambda_arn = module.lambda_origin_response.lambda_function_qualified_arn
    }
  }

  dynamic "custom_error_response" {
    for_each = [
      400,
      403,
      404,
      405,
      414,
      416
    ]
    content {
      error_caching_min_ttl = 10
      error_code            = custom_error_response.value
      response_code         = custom_error_response.value
      response_page_path    = "/error.html"
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  price_class         = "PriceClass_100"
  default_root_object = "index.html"

  viewer_certificate {
    acm_certificate_arn = var.certificate_arn
    ssl_support_method  = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = module.this.tags
}

resource "aws_cloudfront_origin_access_identity" "this" {
  comment = "Origin access identity for www domain access"
}

resource "aws_s3_bucket_policy" "this" {
  provider = aws.operations
  bucket   = var.bucket_id
  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "SharedBucketPolicy"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          "AWS" = aws_cloudfront_origin_access_identity.this.iam_arn
        }
        Action   = "s3:GetObject"
        Resource = "${var.bucket_arn}/*"
    }]
  })
}

module "lambda_origin_request" {
  providers = {
    aws = aws.us_east_1
  }
  source        = "terraform-aws-modules/lambda/aws"
  function_name = "${module.this.id}-lambda-weblite-origin-request"
  description   = "Provide weblite configuration and redirect to index.html"
  handler       = "index.handler"
  runtime       = "nodejs12.x"
  source_path = [
    "${path.module}/lambda-origin-request/",
    var.weblite_config_path,
  ]
  lambda_at_edge                    = true
  cloudwatch_logs_retention_in_days = 14
  cloudwatch_logs_tags              = module.this.tags
  tags                              = module.this.tags
}

module "lambda_origin_response" {
  providers = {
    aws = aws.us_east_1
  }
  source                            = "terraform-aws-modules/lambda/aws"
  function_name                     = "${module.this.id}-lambda-weblite-origin-response"
  description                       = "Add security headers"
  handler                           = "index.handler"
  runtime                           = "nodejs12.x"
  source_path                       = "${path.module}/lambda-origin-response/"
  lambda_at_edge                    = true
  cloudwatch_logs_retention_in_days = 14
  cloudwatch_logs_tags              = module.this.tags
  tags                              = module.this.tags
}
