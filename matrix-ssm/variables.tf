variable "email_domain" {
  type = string
}

variable "kms_key_arn" {
  type = string
}

variable "sygnal_params" {
  type = map(string)
}

variable "sygnal_apps_files" {
  description = "A list containing objects that map from a local file on disk (absolute path) to the name of a file referenced in the sygnal_apps_yaml"
  type = list(object({
    local_path = string,
    name       = string
  }))
}

variable "synapse_params" {
  type = map(string)
}


variable "ma1sd_params" {
  type = map(string)
}

variable "sygnal_metrics_port" {
  type        = string
  description = "The port which sygnal exposes metrics on"
  default     = "9201"
}

variable "matrix_domain" {
  type        = string
  description = "The server part of the users mxids in your server. (e.g., example.com)"
}
variable "matrix_server_fqdn" {
  type        = string
  description = "The public facing domain where clients will access the server (e.g. matrix.example.com)"
}

variable "log_retention_days" {
  type        = number
  description = "the number of days to retain logs in cloudwatch"
  default     = 14
}

variable "log_group_journald" {
  type        = string
  description = "the log group name for journald logs"
}

variable "db_users" {
  type = list(object({
    db       = string
    username = string
    password = string
  }))
}

variable "db_address" {
  type = string
}

variable "db_port" {
  type = number
}
