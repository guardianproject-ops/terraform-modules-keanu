data "aws_region" "current" {}

data "aws_ssm_parameter" "email" {
  name     = local.ssm_email_path
  provider = aws.operations
}

locals {
  ssm_email_path = "/smtp/${var.email_domain}"
  kms_key_arn    = var.kms_key_arn
  aws_region     = data.aws_region.current.id
  smtp_data      = jsondecode(data.aws_ssm_parameter.email.value)
  smtp_params = {
    username = local.smtp_data.smtp_username
    password = local.smtp_data.smtp_password
    server   = local.smtp_data.smtp_server
    port     = local.smtp_data.smtp_port
  }
  synapse_defaults = {
    matrix_domain                       = var.matrix_domain
    matrix_server_fqdn                  = var.matrix_server_fqdn
    default_identity_server             = var.matrix_server_fqdn
    trusted_third_party_id_servers_json = jsonencode([var.matrix_server_fqdn])
    client_port                         = tostring(8008)
    federation_port                     = tostring(8448)
    manhole_port                        = tostring(9000)
    federation_domain_whitelist_json    = jsonencode([])
    metrics_port                        = tostring(9102)
    max_upload_size_mb                  = tostring(1000)
    s3_media_store_region               = local.aws_region
  }
  ma1sd_defaults = {
    http_port = tostring(8090)
  }
  databases = { for o in var.db_users : o.db => {
    db_username = o.username
    db_password = o.password
    db_name     = o.db
    db_hostname = var.db_address
    db_port     = tostring(var.db_port)
  } }
  #synapse_params = merge(local.synapse_defaults,
  #  local.databases.synapse,
  #  var.synapse_params,
  #)
  sygnal_defaults = {
    metrics_port = tostring(9201)
    http_port    = tostring(6000)
  }
  synapse_params = merge(local.synapse_defaults, var.synapse_params, local.databases.synapse)
  sygnal_params  = merge(local.sygnal_defaults, var.sygnal_params)
  ma1sd_params   = merge(local.ma1sd_defaults, var.ma1sd_params, local.databases.ma1sd)
}

module "ssm_prefix" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/3.2.2"
  path_prefix       = "/${module.this.id}"
  prefix_with_label = false
  context           = module.this.context
  region            = local.aws_region
  kms_key_arn       = local.kms_key_arn
}


module "label_log_groups_root" {
  source    = "cloudposse/label/null"
  version   = "0.24.1"
  delimiter = "/"
  context   = module.this.context
}
