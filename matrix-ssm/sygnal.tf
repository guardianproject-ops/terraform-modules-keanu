module "label_sygnal" {
  source     = "cloudposse/label/null"
  version    = "0.24.1"
  context    = module.this.context
  attributes = ["sygnal"]
  tags       = merge(var.tags, { Application = "sygnal" })
}

locals {
  ssm_prefix_sygnal = "${module.ssm_prefix.full_prefix}/sygnal"

  apps_files = [for app_file in var.sygnal_apps_files :
    {
      name : app_file.name
      content : filebase64(app_file.local_path)
    }
  ]
  apps_files_json = jsonencode(local.apps_files)
}

resource "aws_ssm_parameter" "sygnal_params" {
  for_each = local.sygnal_params
  name     = "${local.ssm_prefix_sygnal}/${each.key}"
  type     = "SecureString"
  value    = each.value
  tags     = module.this.tags
}

resource "aws_ssm_parameter" "sygnal__apps_files_json" {
  name      = "${local.ssm_prefix_sygnal}/apps_files_json"
  value     = local.apps_files_json
  type      = "SecureString"
  key_id    = local.kms_key_arn
  tier      = length(local.apps_files_json) > 4000 ? "Advanced" : "Standard"
  overwrite = true
}

module "label_log_sygnal" {
  # we use a second label to use the / delimiter as is custom in cloudwatch
  source    = "cloudposse/label/null"
  version   = "0.24.1"
  delimiter = "/"
  context   = module.label_sygnal.context
}

resource "aws_cloudwatch_log_group" "sygnal" {
  name              = "/${module.label_log_sygnal.id}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "sygnal__log_group_journald" {
  name      = "${local.ssm_prefix_sygnal}/log_group_journald"
  value     = var.log_group_journald
  type      = "SecureString"
  overwrite = true
}

resource "aws_ssm_parameter" "sygnal__log_group_sygnal" {
  name      = "${local.ssm_prefix_sygnal}/log_group_sygnal"
  value     = aws_cloudwatch_log_group.sygnal.name
  type      = "SecureString"
  overwrite = true
}
