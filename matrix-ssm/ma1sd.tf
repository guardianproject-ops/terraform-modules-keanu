module "label_ma1sd" {
  source     = "cloudposse/label/null"
  version    = "0.24.1"
  context    = module.this.context
  attributes = ["ma1sd"]
  tags       = merge(var.tags, { Application = "ma1sd" })
}

locals {
  ssm_prefix_ma1sd = "${module.ssm_prefix.full_prefix}/ma1sd"
}

resource "aws_ssm_parameter" "ma1sd_params" {
  for_each = local.ma1sd_params
  name     = "${local.ssm_prefix_ma1sd}/${each.key}"
  type     = "SecureString"
  value    = each.value
  tags     = module.this.tags
}

# resource "aws_ssm_parameter" "ma1sd__homeserver_internal_uri" {
#   name      = "${local.ssm_prefix_ma1sd}/homeserver_internal_uri"
#   value     = "http://${local.synapse_internal_domain}:${local.synapse_params.client_port}"
#   type      = "SecureString"
#   overwrite = true
#   tags      = module.label_ma1sd.tags
# }

resource "aws_ssm_parameter" "ma1sd__synapse_db__endpoint" {
  name      = "${local.ssm_prefix_ma1sd}/synapse_db_endpoint"
  value     = "${local.synapse_params.db_hostname}:${local.synapse_params.db_port}"
  type      = "SecureString"
  overwrite = true
  tags      = module.label_ma1sd.tags
}

resource "aws_ssm_parameter" "ma1sd__synapse_db__name" {
  name      = "${local.ssm_prefix_ma1sd}/synapse_db_name"
  value     = local.synapse_params.db_name
  type      = "SecureString"
  overwrite = true
  tags      = module.label_ma1sd.tags
}

resource "aws_ssm_parameter" "ma1sd__synapse_db__username" {
  name      = "${local.ssm_prefix_ma1sd}/synapse_db_username"
  value     = local.synapse_params.db_username
  type      = "String"
  overwrite = true
  tags      = module.label_ma1sd.tags
}

resource "aws_ssm_parameter" "ma1sd__synapse_db__password" {
  name      = "${local.ssm_prefix_ma1sd}/synapse_db_password"
  value     = local.synapse_params.db_password
  type      = "SecureString"
  key_id    = local.kms_key_arn
  overwrite = true
  tags      = module.label_ma1sd.tags
}

resource "aws_ssm_parameter" "ma1sd__email__host" {
  name      = "${local.ssm_prefix_ma1sd}/email_smtp_hostname"
  value     = local.smtp_params.server
  type      = "SecureString"
  overwrite = true
  tags      = module.label_ma1sd.tags
}

resource "aws_ssm_parameter" "ma1sd__email__port" {
  name      = "${local.ssm_prefix_ma1sd}/email_smtp_port"
  value     = local.smtp_params.port
  type      = "SecureString"
  overwrite = true
  tags      = module.label_ma1sd.tags
}

resource "aws_ssm_parameter" "ma1sd__email__login" {
  name      = "${local.ssm_prefix_ma1sd}/email_smtp_username"
  value     = local.smtp_params.username
  type      = "SecureString"
  overwrite = true
  tags      = module.label_ma1sd.tags
}

resource "aws_ssm_parameter" "ma1sd__email__password" {
  name      = "${local.ssm_prefix_ma1sd}/email_smtp_password"
  value     = local.smtp_params.password
  type      = "SecureString"
  key_id    = local.kms_key_arn
  overwrite = true
  tags      = module.label_ma1sd.tags
}

module "label_log_ma1sd" {
  # we use a second label to use the / delimiter as is custom in cloudwatch
  source    = "cloudposse/label/null"
  version   = "0.24.1"
  delimiter = "/"
  context   = module.label_ma1sd.context
}

resource "aws_cloudwatch_log_group" "ma1sd" {
  name              = "/${module.label_log_ma1sd.id}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "ma1sd__log_group_journald" {
  name      = "${local.ssm_prefix_ma1sd}/log_group_journald"
  value     = var.log_group_journald
  type      = "SecureString"
  overwrite = true
}

resource "aws_ssm_parameter" "ma1sd__log_group_ma1sd" {
  name      = "${local.ssm_prefix_ma1sd}/log_group_ma1sd"
  value     = aws_cloudwatch_log_group.ma1sd.name
  type      = "SecureString"
  overwrite = true
}
