terraform {
  experiments      = [module_variable_optional_attrs]
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

