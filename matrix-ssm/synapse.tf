module "label_synapse" {
  source     = "cloudposse/label/null"
  version    = "0.24.1"
  context    = module.this.context
  attributes = ["synapse"]
  tags       = merge(var.tags, { Application = "synapse" })
}

locals {
  ssm_prefix_synapse = "${module.ssm_prefix.full_prefix}/synapse"
}

resource "aws_ssm_parameter" "synapse_params" {
  for_each = local.synapse_params

  name      = "${local.ssm_prefix_synapse}/${each.key}"
  value     = each.value
  type      = "SecureString"
  overwrite = true
  tags      = module.this.tags
}

resource "aws_ssm_parameter" "synapse__email_host" {
  name      = "${local.ssm_prefix_synapse}/email_smtp_hostname"
  value     = local.smtp_params.server
  type      = "SecureString"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email_port" {
  name      = "${local.ssm_prefix_synapse}/email_smtp_port"
  value     = local.smtp_params.port
  type      = "SecureString"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email_user" {
  name      = "${local.ssm_prefix_synapse}/email_smtp_username"
  value     = local.smtp_params.username
  type      = "SecureString"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email_password" {
  name      = "${local.ssm_prefix_synapse}/email_smtp_password"
  value     = local.smtp_params.password
  type      = "SecureString"
  key_id    = local.kms_key_arn
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__email__app_name" {
  name      = "${local.ssm_prefix_synapse}/email_app_name"
  value     = var.matrix_domain
  type      = "SecureString"
  key_id    = local.kms_key_arn
  overwrite = true
}

module "label_log_synapse" {
  # we use a second label to use the / delimiter as is custom in cloudwatch
  source    = "cloudposse/label/null"
  version   = "0.24.1"
  delimiter = "/"
  context   = module.label_synapse.context
}

resource "aws_cloudwatch_log_group" "synapse" {
  name              = "/${module.label_log_synapse.id}"
  retention_in_days = var.log_retention_days
}

resource "aws_ssm_parameter" "synapse__log_group_journald" {
  name      = "${local.ssm_prefix_synapse}/log_group_journald"
  value     = var.log_group_journald
  type      = "SecureString"
  overwrite = true
}

resource "aws_ssm_parameter" "synapse__log_group_synapse" {
  name      = "${local.ssm_prefix_synapse}/log_group_synapse"
  value     = aws_cloudwatch_log_group.synapse.name
  type      = "SecureString"
  overwrite = true
}
