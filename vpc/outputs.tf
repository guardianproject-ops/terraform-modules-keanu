output "vpc" {
  value = module.vpc.vpc
}

output "public_subnets" {
  value = module.vpc.public_subnets
}

output "public_subnet_ids" {
  value = [for type, subnet in module.vpc.public_subnets : subnet.subnet_id if type == "public_1" || type == "public_2"]
}

output "private_subnets" {
  value = [for type, subnet in module.vpc.private_subnets : subnet if type == "private_1" || type == "private_2"]
}

output "private_subnet_ids" {
  value = [for type, subnet in module.vpc.private_subnets : subnet.subnet_id if type == "private_1" || type == "private_2"]
}

output "rds_subnets" {
  value = [for type, subnet in module.vpc.private_subnets : subnet if type == "rds_1" || type == "rds_2"]
}

output "rds_subnet_ids" {
  value = [for type, subnet in module.vpc.private_subnets : subnet.subnet_id if type == "rds_1" || type == "rds_2"]
}

output "nat_gateways" {
  value = module.vpc.nat_gateways
}
output "nat_instances" {
  value = module.vpc.nat_instances
}

output "ssh_security_group_id" {
  value = module.vpc.ssh_security_group_id
}

output "availability_zones" {
  value = module.vpc.availability_zones
}

