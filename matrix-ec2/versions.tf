terraform {
  experiments      = [module_variable_optional_attrs]
  required_version = ">= 1.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 3.1"
    }
    aws = {
      source = "hashicorp/aws"
    }
  }
}

