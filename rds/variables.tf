variable "is_prod_like" {
  type        = bool
  description = "Flag to indicate this module is prod like or not. Some behavior will be different in prod like envs"
}

variable "vpc_cidr_block" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "admin_database_name" {
  type = string
}

variable "admin_database_username" {
  type = string
}

variable "admin_database_password" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "allocated_storage" {
  type = number
}


variable "subnet_ids" {
  type = list(string)
}

variable "backup_retention_period" {
  type    = number
  default = 30
}

variable "kms_key_id_override" {
  type        = string
  default     = ""
  description = "override the kms key id used by rds"
}
variable "archive_account_id" {
  type        = string
  description = "The account id for the projects archive account. If specified, the account will be granted access to the rds kms key"
  default     = ""
}
