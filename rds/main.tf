data "aws_caller_identity" "current" {}

locals {
  account_id = data.aws_caller_identity.current.account_id
}

data "aws_iam_policy_document" "default" {
  # this allows the acccount specified by archive_account_id
  # to have read access to our KMS key

  # this first statement is the default iam key policy
  statement {
    sid    = "Enable IAM User Permissions"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${local.account_id}:root"]
    }
    actions = [
      "kms:*",
    ]

    resources = [
      "*"
    ]
  }

  # these following statements re conditionally applied when we have an archive
  # account id
  dynamic "statement" {
    for_each = range(length(var.archive_account_id) > 0 ? 1 : 0)
    content {
      sid    = "AllowArchiveAccount"
      effect = "Allow"
      actions = [
        "kms:Encrypt",
        "kms:Decrypt",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:DescribeKey"
      ]

      principals {
        type        = "AWS"
        identifiers = ["arn:aws:iam::${var.archive_account_id}:root"]
      }

      resources = [
        "*"
      ]
    }
  }

  dynamic "statement" {
    for_each = range(length(var.archive_account_id) > 0 ? 1 : 0)
    content {
      sid    = "AllowArchiveAccountAttachmentOfPersistentResources"
      effect = "Allow"
      actions = [
        "kms:CreateGrant",
        "kms:ListGrants",
        "kms:RevokeGrant"
      ]

      principals {
        type        = "AWS"
        identifiers = ["arn:aws:iam::${var.archive_account_id}:root"]
      }

      resources = [
        "*"
      ]

      condition {
        test     = "Bool"
        variable = "kms:GrantIsForAWSResource"
        values   = [true]
      }
    }
  }
}

module "rds" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-rds-postgresql?ref=tags/0.0.2"

  engine               = "postgres"
  instance_class       = var.instance_class
  engine_version       = "13.4"
  major_engine_version = "13"
  family               = "postgres13"
  allocated_storage    = var.allocated_storage

  # kms
  kms_key_id     = var.kms_key_id_override
  kms_key_policy = data.aws_iam_policy_document.default.json

  # rds meta
  deletion_protection_enabled = var.is_prod_like
  apply_immediately           = true
  skip_final_snapshot         = !var.is_prod_like
  allow_major_version_upgrade = false
  backup_retention_period     = var.backup_retention_period

  # network
  vpc_id         = var.vpc_id
  vpc_cidr_block = var.vpc_cidr_block
  subnet_ids     = var.subnet_ids

  # creds
  database_name     = var.admin_database_name
  database_username = var.admin_database_username
  database_password = var.admin_database_password

  context = module.this.context
}
