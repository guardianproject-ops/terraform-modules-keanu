output "ansible_playbooks_s3_bucket" {
  value = module.ssm_setup.playbooks_bucket_id
}

output "ssm_logs_bucket" {
  value = module.ssm_setup.ssm_logs_bucket
}

output "rotate_ssh" {
  value = module.rotate_ssh
}

output "lambda_rotate_ssh" {
  value = module.rotate_ssh
}

output "secret_prefix" {
  value = local.secret_prefix
}

output "secretsmanager_ssh_key_id" {
  value = aws_secretsmanager_secret.ssh_key.id
}

output "secretsmanager_ssh_key_arn" {
  value = aws_secretsmanager_secret.ssh_key.arn
}

output "log_group_journald_arn" {
  value = aws_cloudwatch_log_group.journald.arn
}

output "log_group_journald_name" {
  value = aws_cloudwatch_log_group.journald.name
}

output "kms_key" {
  value = module.kms_key
}

output "kms_key_arn" {
  value = module.kms_key.key_arn
}

