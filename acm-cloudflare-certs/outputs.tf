output "certs_us_east1" {
  value     = module.cert
  sensitive = true
}

output "certificate_arns_us_east1" {
  value = { for k, o in module.cert : k => o.acm_cert_id }
}

output "certs" {
  value     = module.cert_local
  sensitive = true
}

output "certificate_arns" {
  value = { for k, o in module.cert_local : k => o.acm_cert_id }
}
