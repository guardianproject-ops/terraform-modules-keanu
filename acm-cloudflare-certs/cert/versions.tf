terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 2.19.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0"
    }

  }
  required_version = ">= 1.0"
}
