variable "domain_name" {
  type = string
}
variable "subject_alternative_names" {
  type    = list(string)
  default = []
}

variable "cloudflare_zone_id" {
  type = string
}

variable "validated_hostnames" {
  #type    = list(string)
  default = []
}

variable "is_already_validated" {
  type    = bool
  default = false
}
