provider "cloudflare" {
  api_token = var.cloudflare_edit_token
}

locals {

  certs = {
    "${var.matrix_server_fqdn}" = {
      common_name = var.matrix_server_fqdn
      dns_names   = [var.matrix_server_fqdn, "push.${var.matrix_server_fqdn}"]
    }
    "${var.web_domain}" = {
      common_name = var.web_domain
      dns_names   = []
    }
    "${var.weblite_domain}" = {
      common_name = var.weblite_domain
      dns_names   = []
    }
  }
}


provider "aws" {
  # cloudFront requires that ACM certs be in us-east-1
  alias  = "us_east_1"
  region = "us-east-1"
}

# this cert is in region us_east_1. why?
# to use an ACM certificate with Amazon CloudFront, you must request or import
# the certificate in the US East (N. Virginia) region.
module "cert" {
  for_each                  = local.certs
  source                    = "./cert"
  domain_name               = each.value.common_name
  subject_alternative_names = each.value.dns_names
  cloudflare_zone_id        = var.cloudflare_zone_id

  providers = {
    aws = aws.us_east_1
  }
}

# this cert is in the region of the deployment. why?

# to use a certificate with a load balancer for the same fully qualified domain
# name (FQDN) or set of FQDNs in more than one AWS region, you must request or
# import a certificate for each region. You cannot copy a certificate between
# regions.
module "cert_local" {
  for_each                  = local.certs
  source                    = "./cert"
  domain_name               = each.value.common_name
  subject_alternative_names = each.value.dns_names
  cloudflare_zone_id        = var.cloudflare_zone_id
  is_already_validated      = true
  validated_hostnames       = module.cert[each.key].validated_hostnames
}

# what's up with the validated_hostnames and is_already_validated?
# well we need to validate the cert in muliple regions, but the CNAME validation string
# stays the same across the regions
# this will cause the cloudflare provider to error out with:
#      Error: expected DNS record to not already be present but already exists
# we can't create the same record twice with 2 different terraform resources
# so we just skip the creation of the records in cloudflare if we have succesfully created
# them once before.
# i love this stuff.
